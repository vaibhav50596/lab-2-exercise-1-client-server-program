import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private Socket aSocket;
    private PrintWriter socketOut;
    private BufferedReader socketIn;
    private ServerSocket serverSocket;

    public Server() {
        try {
            serverSocket = new ServerSocket(8099);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

        public void palindrome() {
        String line = null, reverse = "";
        while(true) {
            try {
                line = socketIn.readLine();
                if(line.equalsIgnoreCase("QUIT")) {
                    line = "Good Bye!";
                    socketOut.println("Server shut down!");
                    break;
                }
                int length = line.length();
                for ( int i = length - 1; i >= 0; i-- )
                    reverse = reverse + line.charAt(i);
                if (line.equals(reverse))
                    socketOut.println("Entered string/number is a palindrome.");
                else
                    socketOut.println("Entered string/number isn't a palindrome.");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String [] args) throws IOException{
        Server myServer = new Server();
        //accepting the connection
        try {
            myServer.aSocket = myServer.serverSocket.accept();
            System.out.println("Connection accepted by server");
            myServer.socketIn = new BufferedReader(new InputStreamReader(myServer.aSocket.getInputStream()));
            myServer.socketOut = new PrintWriter(myServer.aSocket.getOutputStream(), true);
            myServer.palindrome();

            myServer.socketOut.close();
            myServer.socketIn.close();

        } catch (IOException e) {

        }
    }
}
